/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.customfields;

import java.util.Date;
import java.util.Map;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.google.common.base.Optional;

import ru.cbgr.jiraworklog.DurationService;
import ru.cbgr.jiraworklog.storage.WorkTrackStorage;


public class CustomTimeSpentField extends GenericTextCFType {
    private static final String ZERO_TIME = "0m";
    private static final long MINUTE = 60L;

    @Inject
    private DurationService durationService;
    @Inject
    private WorkTrackStorage workTrackStorage;

    public CustomTimeSpentField(CustomFieldValuePersister customFieldValuePersister, GenericConfigManager genericConfigManager) {
        super(customFieldValuePersister, genericConfigManager);
    }

    @Nonnull @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);
        String value = ZERO_TIME;
        Optional<Date> startWorkDate = workTrackStorage.get(issue, issue.getAssigneeUser());
        if (startWorkDate.isPresent()) {
            long duration = durationService.getCurrentDate().getTime() - startWorkDate.get().getTime();
            duration = Math.max(toSeconds(duration), MINUTE);
            value = durationService.getShortFormattedDuration(duration);
        }
        params.put("value", value);
        return params;
    }

    private static long toSeconds(long duration) {
        return duration / 1000L;
    }
}
