/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog;

import java.sql.Timestamp;
import java.util.Collection;

import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.google.common.base.Optional;

public class IssueChangeSearchService {

    private static final String IN_PROGRESS_ID = "3";
    private static final String STATUS_FIELD = "status";

    public static Optional<ChangeHistory> findToProgressLastHistoryItem(Collection<ChangeHistory> histories, Timestamp now) {
        ChangeHistory lastToInProgress = null;
        for (ChangeHistory h : histories) {
            if (h.getTimePerformed().before(now)) {
                for (ChangeItemBean itemBean : h.getChangeItemBeans()) {
                    if (STATUS_FIELD.equalsIgnoreCase(itemBean.getField()) && IN_PROGRESS_ID.equals(itemBean.getTo())) {
                        lastToInProgress = max(lastToInProgress, h);
                    }
                }
            }
        }
        return Optional.fromNullable(lastToInProgress);
    }

    private static ChangeHistory max(ChangeHistory ch1, ChangeHistory ch2) {
        if (ch1 == null) {
            return ch2;
        } else if (ch2 == null) {
            return ch1;
        } else {
            return ch1.getTimePerformed().compareTo(ch2.getTimePerformed()) > 0 ? ch1 : ch2;
        }
    }

}
