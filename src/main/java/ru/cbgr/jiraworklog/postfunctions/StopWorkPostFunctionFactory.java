/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.postfunctions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;

import static ru.cbgr.jiraworklog.postfunctions.StopWorkPostFunction.MARKER_FIELD_NAME;
import static ru.cbgr.jiraworklog.postfunctions.StopWorkPostFunction.TIME_SPENT_FIELD_NAME;

public class StopWorkPostFunctionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory {
    private static final String FIELDS = "fields";

    @Inject
    private CustomFieldManager customFieldManager;

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> map) {
        map.put(FIELDS, getFieldNames());
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {
        map.put(FIELDS, getFieldNames());
        map.put(MARKER_FIELD_NAME, getArgValue(abstractDescriptor, MARKER_FIELD_NAME));
        map.put(TIME_SPENT_FIELD_NAME, getArgValue(abstractDescriptor, TIME_SPENT_FIELD_NAME));
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> map, AbstractDescriptor abstractDescriptor) {
        map.put(MARKER_FIELD_NAME, getArgValue(abstractDescriptor, MARKER_FIELD_NAME));
        map.put(TIME_SPENT_FIELD_NAME, getArgValue(abstractDescriptor, TIME_SPENT_FIELD_NAME));
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> params) {
        Map<String, String> result = new HashMap<>();
        if (params.containsKey(MARKER_FIELD_NAME)) {
            result.put(MARKER_FIELD_NAME, extractSingleParam(params, MARKER_FIELD_NAME));
        }
        if (params.containsKey(TIME_SPENT_FIELD_NAME)) {
            result.put(TIME_SPENT_FIELD_NAME, extractSingleParam(params, TIME_SPENT_FIELD_NAME));
        }
        return result;
    }

    private String getArgValue(AbstractDescriptor descriptor, String argName) {
        FunctionDescriptor functionDescriptor = (FunctionDescriptor) descriptor;
        String argValue = (String) functionDescriptor.getArgs().get(argName);
        if (argValue != null && argValue.trim().length() > 0) {
            return argValue;
        } else {
            return "";
        }
    }

    private Map<String, String> getFieldNames() {
        List<CustomField> customFields = customFieldManager.getCustomFieldObjects();
        Map<String, String> fields = new HashMap<>();
        for (CustomField customField : customFields) {
            fields.put(customField.getId(), customField.getFieldName());
        }
        return fields;
    }

}
