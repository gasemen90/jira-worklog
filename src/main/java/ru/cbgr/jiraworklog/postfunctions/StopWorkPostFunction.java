/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.postfunctions;

import java.util.Date;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.SelectCFType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;
import com.opensymphony.workflow.spi.Step;

import ru.cbgr.jiraworklog.CustomWorkLogService;
import ru.cbgr.jiraworklog.DurationService;
import ru.cbgr.jiraworklog.Utils;

public class StopWorkPostFunction extends AbstractJiraFunctionProvider {
    public static final String MARKER_FIELD_NAME = "marker";
    public static final String TIME_SPENT_FIELD_NAME = "timeSpent";

    private static final String COMMENT_FIELD_NAME = "comment";
    private static final String USER_KEY = "userKey";
    private static final String CREATED_STEP = "createdStep";

    @Inject
    private CustomWorkLogService customWorkLogService;
    @Inject
    private CustomFieldManager customFieldManager;
    @Inject
    private DurationService durationService;

    @Override
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        if (!args.containsKey(MARKER_FIELD_NAME) || !args.containsKey(TIME_SPENT_FIELD_NAME)) {
            return;
        }
        Issue issue = super.getIssue(transientVars);
        String workMarker = getWorkMarker(issue, (String) args.get(MARKER_FIELD_NAME));
        String timeSpentStr = getTimeSpent(issue, (String) args.get(TIME_SPENT_FIELD_NAME));
        Step step = (Step) transientVars.get(CREATED_STEP);
        if (Utils.hasNull(workMarker, timeSpentStr, step)) {
            return;
        }
        User user = issue.getAssigneeUser();
        String authorKey = Utils.firstNonNull(transientVars.get(USER_KEY), ApplicationUsers.getKeyFor(user)).toString();
        String comment = Utils.firstNonNull(transientVars.get(COMMENT_FIELD_NAME), "").toString();
        String markedComment = String.format("[%s] %s", workMarker, comment);
        Date logDate = step.getStartDate();
        long timeSpent = durationService.parseDuration(timeSpentStr);
        customWorkLogService.createWorkLog(issue, user, authorKey, markedComment, logDate, timeSpent);
    }

    private String getWorkMarker(Issue issue, String id) {
        CustomField workMarkerField = customFieldManager.getCustomFieldObject(id);
        if (workMarkerField == null || !SelectCFType.class.isInstance(workMarkerField.getCustomFieldType())) {
            return null;
        }
        Option workMarker = (Option) issue.getCustomFieldValue(workMarkerField);
        return workMarker.getValue();
    }

    private String getTimeSpent(Issue issue, String id) {
        CustomField timeSpentField = customFieldManager.getCustomFieldObject(id);
        if (timeSpentField == null) {
            return null;
        }
        return (String) issue.getCustomFieldValue(timeSpentField);
    }
}
