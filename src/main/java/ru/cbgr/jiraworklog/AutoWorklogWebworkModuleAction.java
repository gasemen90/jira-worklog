/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog;

import java.text.MessageFormat;
import java.util.Date;

import javax.inject.Inject;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import org.springframework.stereotype.Component;

import static com.atlassian.jira.bc.issue.IssueService.IssueResult;

@Component
public class AutoWorklogWebworkModuleAction extends JiraWebActionSupport {

    @Inject
    private IssueService issueService;
    @Inject
    private JiraAuthenticationContext authenticationContext;
    @Inject
    private CustomWorkLogService workLogService;
    @Inject
    private DurationService durationService;

    private Long id;
    private String worklogMarker;
    private String worklogTime;
    private String worklogComment;

    @Override // вызывается при submit'е
    protected void doValidation() {
        super.doValidation();
    }

    @Override
    protected String doExecute() throws Exception {
        String returnUrl = null;
        Issue issue = getIssueObject();
        if (issue != null) {
            returnUrl = "/browse/" + issue.getKey();
            ApplicationUser user = authenticationContext.getUser();
            String msg = MessageFormat.format("[{0}] {1}", worklogMarker, worklogComment);
            Date currDate = durationService.getCurrentDate();
            Long duration = durationService.parseDuration(worklogTime);
            workLogService.createWorkLog(issue, user.getDirectoryUser(), user.getKey(), msg, currDate, duration);
        }

        return returnComplete(returnUrl);
    }

    @Override // вызывается при открытии формы
    public String doDefault() throws Exception {
        return super.doDefault();
    }

    private Issue getIssueObject() {
        IssueResult issueResult = issueService.getIssue(authenticationContext.getUser().getDirectoryUser(), id);
        if (!issueResult.isValid()) {
            addErrorCollection(issueResult.getErrorCollection());
            return null;
        }

        return issueResult.getIssue();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorklogMarker() {
        return worklogMarker;
    }

    public void setWorklogMarker(String worklogMarker) {
        this.worklogMarker = worklogMarker;
    }

    public String getWorklogTime() {
        return worklogTime;
    }

    public void setWorklogTime(String worklogTime) {
        this.worklogTime = worklogTime;
    }

    public String getWorklogComment() {
        return worklogComment;
    }

    public void setWorklogComment(String worklogComment) {
        this.worklogComment = worklogComment;
    }
}
