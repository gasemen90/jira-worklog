/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.listeners;

import java.util.List;

import javax.inject.Inject;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.cbgr.jiraworklog.CustomWorkLogService;

import static ru.cbgr.jiraworklog.IssueChangeSearchService.findToProgressLastHistoryItem;

public class IssueProgressStatusListener extends AbstractIssueEventListener {
    private static final Logger logger = LoggerFactory.getLogger(IssueProgressStatusListener.class);

    private static final String MESSAGE = "Automated WorkLog";

    private static final List<Long> EVENT_TYPE_IDS = ImmutableList.of(
            EventType.ISSUE_CLOSED_ID,
            EventType.ISSUE_RESOLVED_ID,
            EventType.ISSUE_WORKSTOPPED_ID);

    @Inject
    private ChangeHistoryManager changeHistoryManager;

    @Inject
    private CustomWorkLogService customWorkLogService;

    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        GenericValue changeLog = issueEvent.getChangeLog();

        if (!EVENT_TYPE_IDS.contains(eventTypeId) || !changeLog.containsKey("id")) {
            return;
        }

        Long changeLogId = changeLog.getLong("id");
        Issue issue = issueEvent.getIssue();
        User user = issue.getAssigneeUser();
        List<ChangeHistory> histories = Lists.reverse(changeHistoryManager.getChangeHistoriesForUser(issue, user));

        ChangeHistory eventChange = null;
        for (ChangeHistory history : histories) {
            if (history.getId().equals(changeLogId)) {
                eventChange = history;
                break;
            }
        }

        if (eventChange != null) {
            Optional<ChangeHistory> lastInProgress = findToProgressLastHistoryItem(histories, eventChange.getTimePerformed());
            if (lastInProgress.isPresent()) {
                logger.debug("Log work for issue {}", issue.getKey());
                DateTime startDate = new DateTime(lastInProgress.get().getTimePerformed());
                DateTime eventDate = new DateTime(eventChange.getTimePerformed());
                long diff = Seconds.secondsBetween(startDate, eventDate).getSeconds();
                customWorkLogService.createWorkLog(issue, user, eventChange.getAuthorKey(), MESSAGE, eventDate.toDate(), diff);
            }
        }
    }
}
