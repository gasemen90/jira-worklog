/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.listeners;

import java.util.List;

import javax.inject.Inject;

import com.atlassian.event.api.EventListener;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.Issue;
import com.google.common.collect.ImmutableList;

import ru.cbgr.jiraworklog.storage.WorkTrackStorage;

public class WorkStartedStoppedListener extends AbstractIssueEventListener {

    @Inject
    private WorkTrackStorage workTrackStorage;

    private static final List<Long> STOP_EVENT_TYPE_IDS = ImmutableList.of(
            EventType.ISSUE_DELETED_ID,
            EventType.ISSUE_CLOSED_ID,
            EventType.ISSUE_RESOLVED_ID,
            EventType.ISSUE_WORKSTOPPED_ID);

    @EventListener
    public void onIssueEvent(IssueEvent issueEvent) {
        Long eventTypeId = issueEvent.getEventTypeId();
        Issue issue = issueEvent.getIssue();
        if (EventType.ISSUE_WORKSTARTED_ID.equals(eventTypeId)) {
            workTrackStorage.store(issue, issue.getAssigneeUser(), issueEvent.getTime());
        } else if (STOP_EVENT_TYPE_IDS.contains(eventTypeId)) {
            workTrackStorage.remove(issue, issue.getAssigneeUser());
        }
    }
}
