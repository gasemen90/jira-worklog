/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog;

import java.util.Date;

import javax.inject.Inject;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.worklog.WorklogResult;
import com.atlassian.jira.bc.issue.worklog.WorklogResultFactory;
import com.atlassian.jira.bc.issue.worklog.WorklogService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogImpl;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.user.ApplicationUsers;

import org.springframework.stereotype.Component;

@Component
public class CustomWorkLogService {

    @Inject
    private WorklogManager worklogManager;

    @Inject
    private WorklogService worklogService;

    public Worklog createWorkLog(Issue issue, User user, String authorKey, String message, Date eventDate, Long timeSpentInSeconds) {
        Worklog worklog = newWorkLog(issue, authorKey, message, eventDate, timeSpentInSeconds);
        WorklogResult worklogResult = WorklogResultFactory.create(worklog, false);
        JiraServiceContextImpl serviceContext = new JiraServiceContextImpl(ApplicationUsers.from(user));
        return worklogService.createAndAutoAdjustRemainingEstimate(serviceContext, worklogResult, true);
    }

    private Worklog newWorkLog(Issue issue, String userKey, String message, Date eventDate, Long timeSpentInSeconds) {
        return new WorklogImpl(worklogManager, issue, null, userKey, message, eventDate, null, null, timeSpentInSeconds, null, null, null);
    }

}
