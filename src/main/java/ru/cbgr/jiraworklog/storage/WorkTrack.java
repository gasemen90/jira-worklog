/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.storage;

import java.util.Date;

import net.java.ao.Accessor;
import net.java.ao.Entity;
import net.java.ao.Mutator;
import net.java.ao.schema.Table;

@Table("WORK_TRACK")
public interface WorkTrack extends Entity {
    String PROJECT_KEY = "PROJECT_KEY";
    String USER_KEY = "USER_KEY";
    String ISSUE_KEY = "ISSUE_KEY";
    String START_WORK_DATE = "START_WORK_DATE";

    @Accessor(PROJECT_KEY)
    String getProjectKey();

    @Mutator(PROJECT_KEY)
    void setProjectKey(String key);

    @Accessor(USER_KEY)
    String getUserKey();

    @Mutator(USER_KEY)
    void setUserKey(String key);

    @Accessor(ISSUE_KEY)
    String getIssueKey();

    @Mutator(ISSUE_KEY)
    void setIssueKey(String key);

    @Accessor(START_WORK_DATE)
    Date getaStartWorkDate();

    @Mutator(START_WORK_DATE)
    void setStartWorkDate(Date startWorkDate);
}
