/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.storage;

import java.util.Date;

import javax.inject.Inject;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUsers;
import com.google.common.base.Optional;

import net.java.ao.Query;

import org.springframework.stereotype.Service;

@Service
public class WorkTrackStorageImpl implements WorkTrackStorage {
    private static final String FIND_BY_KEY = "PROJECT_KEY = ? AND USER_KEY = ? AND ISSUE_KEY = ?";

    @Inject
    private ActiveObjects ao;

    @Override
    public void store(Issue issue, User user, Date date) {
        Optional<WorkTrack> wl = find(issue, user);
        WorkTrack workTrack;
        if (wl.isPresent()) {
            workTrack = wl.get();
        } else {
            workTrack = ao.create(WorkTrack.class);
            workTrack.setProjectKey(issue.getProjectObject().getKey());
            workTrack.setUserKey(ApplicationUsers.getKeyFor(user));
            workTrack.setIssueKey(issue.getKey());
        }
        workTrack.setStartWorkDate(date);
        workTrack.save();
    }

    @Override
    public Optional<Date> get(Issue issue, User user) {
        Optional<WorkTrack> workTrack = find(issue, user);
        Date startWorkDate = workTrack.isPresent() ? workTrack.get().getaStartWorkDate() : null;
        return Optional.fromNullable(startWorkDate);
    }

    @Override
    public void remove(Issue issue, User user) {
        Optional<WorkTrack> workTrack = find(issue, user);
        if (workTrack.isPresent()) {
            ao.delete(workTrack.get());
        }
    }

    private Optional<WorkTrack> find(Issue issue, User user) {
        WorkTrack[] workTracks = ao.find(WorkTrack.class, Query.select().where(FIND_BY_KEY,
                issue.getProjectObject().getKey(),
                ApplicationUsers.getKeyFor(user),
                issue.getKey()));
        WorkTrack workTrack = workTracks.length > 0 ? workTracks[0] : null;
        return Optional.fromNullable(workTrack);
    }
}
