/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog.storage;

import java.util.Date;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.google.common.base.Optional;

@Transactional
public interface WorkTrackStorage {

    void store(Issue issue, User user, Date date);

    Optional<Date> get(Issue issue, User user);

    void remove(Issue issue, User user);
}
