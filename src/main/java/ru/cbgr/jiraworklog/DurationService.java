/*
 * Copyright (c) 2017, TopS BI LLC. All rights reserved.
 * http://www.topsbi.ru
 */

package ru.cbgr.jiraworklog;

import java.util.Date;

import javax.inject.Inject;

import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.util.JiraDurationUtils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.stereotype.Component;

@Component
public class DurationService {

    @Inject
    private JiraAuthenticationContext authenticationContext;
    @Inject
    private JiraDurationUtils durationUtils;
    @Inject
    private TimeZoneManager timeZoneManager;

    public Long parseDuration(String duration) {
        try {
            return durationUtils.parseDuration(duration, authenticationContext.getLocale());
        } catch (InvalidDurationException e) {
            throw new RuntimeException(e);
        }
    }

    public String getShortFormattedDuration(long duration) {
        return durationUtils.getShortFormattedDuration(duration, authenticationContext.getLocale());
    }

    public Date getCurrentDate() {
        DateTimeZone timeZone = DateTimeZone.forTimeZone(timeZoneManager.getDefaultTimezone());
        return new DateTime(timeZone).toDate();
    }
}
